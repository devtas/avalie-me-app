var express = require('express');
var mongoose = require('mongoose');
var Account = mongoose.model('Account');
var Project = mongoose.model('Project');
var Client = mongoose.model('Client');
var totals = {
  projects: 0,
  clients: 0
};

exports.index = function(req, res) {
  Account.find(req.user['_id']).exec(function(err, user) {
    Client.find({
      user: req.user['_id']
    }).exec(function(err, clients) {
      totals.clients = clients.length;
      Project.find({
        user: req.user['_id']
      }).exec(function(err, projects) {
        totals.projects = projects.length;
        res.render('app/index', {
          title: 'Dashboard',
          page: 'index',
          userData: user[0],
          totals: totals
        });
      });
    });
  });
}


exports.profile = function(req, res) {
  Account.find(req.user['_id']).exec(function(err, user) {
    Client.find({
      user: req.user['_id']
    }).exec(function(err, clients) {
      Project.find({
        user: req.user['_id']
      }).exec(function(err, projects) {
        res.render('app/profile', {
          title: 'Perfil',
          page: 'profile',
          userData: user[0],
          projects: projects,
          clients: clients
        });
      });
    });
  });
}


exports.projects = function(req, res) {
  Account.find(req.user['_id']).exec(function(err, user) {
    Project.find({
      user: req.user['_id']
    })
    .populate('client')
    .exec(function(err, projects) {
      console.info(projects);
      res.render('app/projects', {
        title: 'Projetos',
        unit: 'projeto',
        page: 'projects',
        userData: user[0],
        projects: projects
      });
    });
  });
}


exports.viewProject = function(req, res) {
  Project.findOne({
    _id: req.params.id,
    user: req.user._id
  })
  .populate('client')
  .exec(function(err, project) {
    console.info(project);
    res.render('app/project/view', {
      title: project.name,
      unit: 'projeto',
      userData: req.user,
      project: project
    });
  });
}


exports.clients = function(req, res) {
  Account.find(req.user['_id']).exec(function(err, user) {
    Client.find({
      user: req.user['_id']
    }).exec(function(err, clients) {
      res.render('app/clients', {
        title: 'Clientes',
        unit: 'cliente',
        page: 'clients',
        userData: user[0],
        clients: clients
      });
    });
  });
}


// Create new
exports.new = {
  // Create New
  project: function(req, res) {
    Client.find({
      user: req.user['_id']
    }).exec(function(err, clients) {
      res.render('app/project/new', {
        title: 'Adicionar projeto',
        dados: new Project({}),
        userData: req.user,
        clients: clients
      });
    });
  },
  saveProject: function(req, res) {
    var obj = new Project(req.body);
    obj.user = req.user._id;
    obj.save(function(err, i) {
      console.info("err", err);
      console.info("i", i);
      if (req.body['done']) {
        res.redirect(req.body.done);
      } else {
        //res.redirect('/app/projects/' + i._id);
        res.redirect('/app/projects/');
      }
    });
  },
  client: function(req, res) {
    res.render('app/client/new', {
      title: 'Adicionar cliente',
      dados: new Client({}),
      userData: req.user
    });
  },
  saveClient: function(req, res) {
    var obj = new Client(req.body);
    obj.user = req.user._id;
    obj.save(function(err, i) {
      console.info("err", err);
      console.info("i", i);
      if (req.body['done']) {
        res.redirect(req.body.done);
      } else {
        res.redirect('/app/clients/');
      }
    });
  }
}

// Edits
exports.edit = {
  client: function(req, res) {
    Client.findOne({
      _id: req.params.id,
      user: req.user._id
    }).exec(function(err, client) {
      res.render('app/client/edit', {
        title: 'Editar cliente',
        userData: req.user,
        dados: client
      });
    });
  },
  updateClient: function(req, res) {
    Client.update({
      _id: req.body.client_id,
      user: req.user._id
    }, {
      name: req.body.name,
      website: req.body.website,
      area: req.body.area,
      responsible: req.body.responsible,
      email: req.body.email,
      phone: req.body.phone
    }, function(err, i) {
      if (!err) {
        return res.redirect('/app/clients');
      }
      res.send('Error');
    });
  },
  project: function(req, res) {
    Project.findOne({
      _id: req.params.id,
      user: req.user._id
    }).exec(function(err, project) {
      Client.find({
        user: req.user._id
      }).exec(function(err, clients) {
        res.render('app/project/edit', {
          title: 'Editar projeto',
          userData: req.user,
          dados: project,
          clients: clients
        });
      });
    });
  },
  updateProject: function(req, res) {
    Project.update({
      _id: req.body.project_id,
      user: req.user._id
    }, {
      name: req.body.name,
      responsible: req.body.responsible,
      price: req.body.price
    }, function(err, i) {
      if (!err) {
        return res.redirect('/app/projects/');
      }
      res.send('Error');
    });
  }
}


exports.delete = {
  // Delete
  client: function(req, res) {
    Client.remove({
      _id: req.body.client_id,
      user: req.user._id
    }, function(err, i) {
      if (!err) {
        return res.redirect('/app/clients');
      }
      res.send('Erro ao deletar!');
    });
  },
  project: function(req, res) {
    Project.remove({
      _id: req.body.project_id,
      user: req.user._id
    }, function(err, i) {
      if (!err) {
        return res.redirect('/app/projects');
      }
      res.send('Erro ao deletar!');
    });
  }
}