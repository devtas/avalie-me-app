
// Admin Functions
var express = require('express');
var mongoose = require('mongoose');
var Account = mongoose.model('Account');
var Plan = mongoose.model('Plan');
var router = express.Router();

exports.index = function(req, res) {
  Account.find().exec(function(err, users) {
	  Plan.find().exec(function(err, plans) {
	    res.render('admin/index', {
	      title: "Avalie-me Admin",
	      users: users,
	      plans: plans
	    });
	  });
  });
}