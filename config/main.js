var path = require('path');
var rootPath = path.normalize(__dirname + '/..');

var user_status = [
    'active',
    'canceled',
    'suspended',
    'freetrial'
  ];

module.exports = {
  development: {
    db: 'mongodb://localhost/avaliemeapp',
    root: rootPath,
    app: {
      name: 'Avalie-me App'
    },
    user_status: user_status
  },
  production: {
    db: 'mongodb://localhost/avaliemeapp',
    root: rootPath,
    app: {
      name: 'Avalie-me App Prod'
    },
    user_status: user_status
  }

}
