var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var AccountSchema = new Schema({
  name: {
    type: String,
    default: ''
  },
  url: {
    type: String
  },
  website: {
    type: String,
    default: '',
  },
  company: {
    type: String,
    default: '',  
  },
  address: {
    type: String,
    default: '',  
  },
  country: {
    type: String,
    default: '',  
  },
  city: {
    type: String,
    default: '',  
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  created_at: {
    default: Date.now,
    type: Date
  },
  type: {
    default: 'normal',
    type: String
  },
  status: {
    default: 'active',
    type: String
  },
  plan: {
    type: Schema.ObjectId,
    ref: 'Plan'
  },
  twitter: {
    type: String,
    default: '', 
  },
  facebook: {
    type: String,
    default: '', 
  },
  linkedin: {
    type: String,
    default: '', 
  },
  google_plus: {
    type: String,
    default: '', 
  },
  youtube: {
    type: String,
    default: '', 
  },
  instagram: {
    type: String,
    default: '', 
  }
});

AccountSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', AccountSchema);
