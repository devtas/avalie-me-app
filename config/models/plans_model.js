
var mongoose = require('mongoose')

var PlanSchema = new mongoose.Schema({
  name: { type: String, default: '' },
  user_limit: { type: Number, default: 0 },
  price: { type: Number, default: 0 }
});

mongoose.model('Plan', PlanSchema);