var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'Account',
    required: true
  },
  name: {
    type: String,
    default: '',
    required: true
  },
  website: {
    type: String,
    default: '',
  },
  area: {
    type: String,
    default: '',
  },
  email: {
    type: String,
    default: '',
  },
  phone: {
    type: String,
    default: '',
  },
  responsible: {
    type: String,
    default: ''
  }
});

mongoose.model('Client', ClientSchema);