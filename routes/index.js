var express = require('express');
var router = express.Router();
var site = require('../controllers/site');
var admin = require('../controllers/admin');


/* GET home page. */
router.get('/', site.index);

/* GET sobre. */
router.get('/sobre', site.about);

/* GET depoimentos. */
router.get('/depoimentos', site.testimonials);

/* GET preços. */
router.get('/precos', site.prices);

/* GET contato. */
router.get('/contato', site.contact);

/* GET Signup. */
//router.get('/signup', site.signup);

/* GET Login. */
//router.get('/login', site.login);

/* GET Admin. */
router.get('/admin', admin.index);

module.exports = router;
