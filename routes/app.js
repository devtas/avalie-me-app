var express = require('express');
var router = express.Router();
var app = require('../controllers/app');

/* GET app page. */
router.get('/', auth, app.index);

router.get('/profile', auth, app.profile);


// Projects
router.get('/projects', auth, app.projects);
router.get('/project/:id', auth, app.viewProject); // View Project
router.get('/projects/new', auth, app.new.project);
router.post('/projects/new/save', auth, app.new.saveProject);
router.get('/project/edit/:id', auth, app.edit.project); // Edit Project
router.post('/project/edit/save', auth, app.edit.updateProject); // Save Edited Project
router.post('/projects/delete', auth, app.delete.project); // Delete Project


// Clients
router.get('/clients', auth, app.clients); // List Clients
router.get('/clients/new', auth, app.new.client); // New Client
router.post('/clients/new/save', auth, app.new.saveClient); // Save New Client
router.get('/clients/edit/:id', auth, app.edit.client); // Edit Client
router.post('/clients/edit/save', auth, app.edit.updateClient); // Save Edited Client
router.post('/clients/delete', auth, app.delete.client); // Delete Client

module.exports = router;

function auth(req, res, next) {
  if (!req.isAuthenticated()) {
    return res.redirect('/login');
  }
  next();
}
