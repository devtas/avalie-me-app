var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var fs = require('fs');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var _ = require('lodash');

var app = express();

// Load configurations
// if test env, load example file
var env = process.env.NODE_ENV || 'development';
var config = require('./config/main')[env];

// Bootstrap db connection
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// Bootstrap models
var models_path = __dirname + '/config/models';
fs.readdirSync(models_path).forEach(function(file) {
  if (~file.indexOf('.js')) require(models_path + '/' + file)
})

//var AccountStatus = mongoose.model('Account');
//console.info('Account Schema is loaded!');

// Set Routes After Mongoose Model
var routes = require('./routes/index');
var appRoutes = require('./routes/app');

// For Jade Date Formats
app.locals.moment = require('moment');
app.locals.moment.locale('pt-br');
app.locals._ = _;
app.locals.config = config;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/img/favicon.png'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({
  secret: 'hello@kpax!roadapps',
  resave: false,
  saveUninitialized: true,
  store: new MongoStore({
    url: config.db,
    ttl: 1200000 // 20 minutes
  }),
  cookie: {
    maxAge: 1200000 // 20 minutes
  }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

var Account = require('./config/models/auth_model');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

app.use('/', routes);
app.use('/app', appRoutes);

app.get('/login', function(req, res) {
  if (req['user']) {
    res.redirect('/app/')
  } else {
    res.render('login', {
      title: 'Acessar',
      page: 'login'
    });
  }
});

app.post('/login', passport.authenticate('local'), function(req, res) {
  res.redirect('/app/');
});

app.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/login');
});

app.get('/signup', function(req, res) {
  res.render('signup', {
    title: 'Cadastrar',
    page: 'signup'
  });
});

app.post('/signup', function(req, res) {
  Account.register(new Account({
    name: req.body.name,
    username: req.body.username,
    email: req.body.email
  }), req.body.password, function(err, account) {
    if (err) {
      console.log('signup err', err);
      return res.render('signup', {
        account: account
      });
    }
    fs.mkdir(path.join(__dirname, 'public/uploads/' + account._id), function(err) {
      passport.authenticate('local')(req, res, function() {
        res.redirect('/app');
      });
    });
  });
});

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;