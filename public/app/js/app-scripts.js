
$(document).ready(function() {
  // Modal to Delete Client
  $('#modal-delete-client').on('show.bs.modal', function (event) {
    // Collecting values
    var button    = $(event.relatedTarget)
    var id        = button.data('client-id')
    var name      = button.data('name')
    var modal     = $(this)

    // Putting values
    modal.find('.modal-title').text('Excluir ' + name)
    modal.find('.client-id').attr('value', id)
  })

  // Modal to Delete Project
  $('#modal-delete-project').on('show.bs.modal', function (event) {
    // Collecting values
    var button    = $(event.relatedTarget)
    var id        = button.data('project-id')
    var name      = button.data('name')
    var modal     = $(this)

    // Putting values
    modal.find('.modal-title').text('Excluir: ' + name)
    modal.find('.project-id').attr('value', id)
  })
});