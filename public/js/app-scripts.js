
// define angular Form module/app
angular.module('formSignup', []).controller('signupController', ['$scope', '$http', function SignUpController($scope, $http) {
  // create a blank object to hold our form information
  // $scope will allow this to pass between controller and view
  $scope.formData = {};

  // processing the form
  $scope.processForm = function() {
    // POSTS data to webservice
    $http.post('/signup', $scope.formData).
    success(function(data) {
      $scope.error = false;
      $scope.data = data;
    }).
    error(function(data) {
      $scope.error = true;
      $scope.data = data;
    });
  };
}]);